Para compilar el código en Linux, adjuntamos un script en shell llamado "compila.sh".
Ubicar los archivos main.cpp, funciones.cpp, funciones.h, TFloat.cpp y TFloat.h en el mismo directorio que el script, y ejecutarlo.

El ejecutable obtenido, "metnumtp1", puede ser utilizado siguiendo el formato indicado a continuación:

'metnumtp1 FILENAME PRECISION {biseccion A B | newton ESTIMACION} MAXPASOS TOLERANCIA'

FILENAME	El nombre del archivo con la muestra. Debe estar ubicado en el mismo directorio que el ejecutable.
PRECISION	Un valor entero entre 1 y 51 que representa los dígitos de precisión en la mantisa.
A, B		Extremos iniciales para el método de Bisección. (Sólo si antes se ingresó el parámetro 'biseccion').
ESTIMACION	La estimación inicial para el método de Newton. (Sólo si antes se ingresó el parámetro 'newton').
MAXPASOS	El máximo de iteraciones a realizar por el programa antes de devolver una solución.
TOLERANCIA	El grado deseado de tolerancia en el resultado.