function graficarTodo()
	
	a = csvread("valores.csv");
	sigmas = a(2:55,6);
	betas = a(2:55,7);
	lambdas = a(2:55,8);
	datas = { "x_15_9_3", "x_2_62_35", "X1", "X2", "X3", "X4", "X5", "X6", "X7" };
	for i = 1:3:54
		sigma1 = sigmas(i);
		beta1 = betas(i);
		lambda1 = lambdas(i);
		sigma2 = sigmas(i+1);
		beta2 = betas(i+1);
		lambda2 = lambdas(i+1);
		sigma3 = sigmas(i+2);
		beta3 = betas(i+2);
		lambda3 = lambdas(i+2);
		file = datas{ceil(i/6)};
		data = leer_datos([file".txt"]);
		dibujarHistyAjuste(data, sigma1,beta1,lambda1, sigma2, beta2, lambda2, sigma3, beta3, lambda3);
		png = [file "_" int2str(((1 + ((-1)^i)) / 2) + 1) ".png"];
		print("-dpng", png);
		close all;
	end