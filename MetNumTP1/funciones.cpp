/* 
 * File:   funciones.cpp
 * Author: nahdelgado
 *
 * Created on 31 de marzo de 2013, 22:22
 */

#include "funciones.h"

using namespace std;

TFloat funcM(TFloat beta, TFloat *muestra, int n) {
	TFloat ac = TFloat(0, beta.precision());
        int i = 0;
    
	while(i < n) {
        double po = pow(muestra[i].dbl(), beta.dbl());
        ac = ac + TFloat(po, beta.precision());
        i++;
	}
        
        ac = ac / n;
        
        return ac;
}

TFloat funcMp(TFloat beta, TFloat *muestra, int n) {
	TFloat ac = TFloat(0, beta.precision());
	int i = 0;
    
	while(i < n) {
        TFloat fM1 = TFloat(pow(muestra[i].dbl(), beta.dbl()), beta.precision());
        fM1 = fM1 * TFloat(log(muestra[i].dbl()), muestra[i].precision());
        ac = ac + fM1;
        i++;
	}
        
        ac = ac / n;
        
        return ac;
}

TFloat funcMs(TFloat beta, TFloat *muestra, int n) {
	TFloat ac = TFloat(0, beta.precision());
	int i = 0;
    
	while(i < n) {
        TFloat lnXi = TFloat(log(muestra[i].dbl()), muestra[i].precision());
        TFloat fM2 = TFloat(pow(muestra[i].dbl(), beta.dbl()), beta.precision());
        fM2 = lnXi * lnXi * fM2;
        ac = ac + fM2;
        i++;
	}
        
        ac = ac / n;
        
        return ac;
}

TFloat funcR(TFloat beta, TFloat *muestra, int n){
    TFloat fM, fM1, res;
    
    fM1 = funcMp(beta, muestra, n);
    fM = funcM(beta, muestra, n);
        
    res = fM1 / fM;
    
    return res;
}

TFloat funcRp(TFloat beta, TFloat *muestra, int n){
    TFloat fM, fM1, fM2, res;
    
    fM2 = funcMs(beta, muestra, n);
    fM = funcM(beta, muestra, n);
    fM1 = funcMp(beta, muestra, n);
    
    res = (fM2 * fM - fM1*fM1) / (fM * fM);
    
    return res;
}

TFloat funcF(TFloat beta, TFloat *muestra, int n) {
    TFloat fRb, fR0, fM2b, fMb, res;
    
    fRb = funcR(beta, muestra, n);
    fR0 = funcR(TFloat(0, beta.precision()), muestra, n);
    fM2b = funcM(beta * 2, muestra, n);
    fMb = funcM(beta, muestra, n);   
    
    res = beta * (fRb - fR0) + 1;  
    double lo = log(res.dbl());
    res = TFloat(lo, beta.precision());
    res = res - log(fM2b.dbl());
    res = res + log(fMb.dbl()) * 2;
    
    return res;
}

TFloat funcFp(TFloat beta, TFloat *muestra, int n) {
    TFloat fRb, fR0, fRpb, fM2b, fMp2b, fMb, fMpb, res;
    
    fRb = funcR(beta, muestra, n);
    fR0 = funcR(TFloat(0, beta.precision()), muestra, n);
    fRpb = funcRp(beta, muestra, n);
    fM2b = funcM(beta * 2, muestra, n);
    fMp2b = funcMp(beta * 2, muestra, n);
    fMb = funcM(beta, muestra, n);
    fMpb = funcMp(beta, muestra, n);
    
    res = (fRb - fR0 + beta * fRpb) / (beta *(fRb - fR0) + 1);
    res = res - (fMp2b * 2) / fM2b;
    res = res + (fMpb * 2) / fMb;
    
    return res;
}

TFloat bisection(TFloat a, TFloat b, TFloat *muestra, int n, double tolerancia, int& maxPasos) {
    int i = 0;
    TFloat fa = funcF(a, muestra, n);
    TFloat fp = funcF(b, muestra, n);
    TFloat p = TFloat(0, a.precision());
    
    TFloat anterior = TFloat(0, a.precision());
    
    if((fa*fp).dbl() > 0) {
        maxPasos = i;
        return 0;
    }
    
    while(i < maxPasos) {
        p = a + (b - a)/2;
        fa = funcF(a, muestra, n);
        fp = funcF(p, muestra, n);
        i++;
        double fpMod = fp.dbl() > 0 ? fp.dbl() : -fp.dbl();
        if(fpMod < tolerancia || isnan(p.dbl()) || isnan(fpMod)) {
            break;
        }
        if((fa * fp).dbl() > 0) {
            a = p;
        }
        else {
            b = p;
        }
        
        if(anterior.dbl() == p.dbl()) // Sabemos que no es correcto comparar doubles sin una tolerancia
        {                                // pero la idea es justamente frenar cuando, por la precision que tengo,
            break;                       // no estoy acercandome más al cero
        }
            
            anterior = p;
    }
    
    maxPasos = i;
            
    return p;
}

TFloat newton(TFloat beta, TFloat *muestra, int n, double tolerancia, int &maxPasos) {
    TFloat fF, fFp, anterior, temp;
          
    int i = 0;   
    anterior = beta;
    
    fF = funcF(beta, muestra, n);
    fFp = funcFp(beta, muestra, n);
    
    while(i < maxPasos) 
    {
        temp = fF / fFp;
        temp = beta - temp;
        beta = temp;            
        i++;
        
        if((fF.dbl() > 0 ? fF.dbl() : -fF.dbl()) < tolerancia || isnan(beta.dbl()))
        {
            break;
        }
        
        if(anterior.dbl() == beta.dbl()) // Sabemos que no es correcto comparar doubles sin una tolerancia
        {                                // pero la idea es justamente frenar cuando, por la precision que tengo,
            break;                       // no estoy acercandome más al cero
        }
        
        anterior = beta;
        fF = funcF(beta, muestra, n);
        fFp = funcFp(beta, muestra, n);
    }    
    
    maxPasos = i;
    return beta;
}

TFloat funcLambda(TFloat beta, TFloat *muestra, int n)
{
    TFloat lambda = TFloat(0, beta.precision());
    
    TFloat fM = funcM(beta, muestra, n);
    TFloat fM1 = funcMp(beta, muestra, n);
    TFloat fM10 = funcMp(TFloat(0, beta.precision()), muestra, n);
    
    lambda = beta * ((fM1 / fM) - fM10);
    lambda = TFloat(pow(lambda.dbl(), -1), lambda.precision());
    
    return lambda;
}

TFloat funcSigma(TFloat beta, TFloat lambda, TFloat *muestra, int n)
{
    TFloat sigma = TFloat(0, beta.precision());
    
    TFloat fM = funcM(beta, muestra, n);
    
    sigma = fM / lambda;
    sigma = TFloat(pow(sigma.dbl(), 1 / beta.dbl()), sigma.precision());
    
    return sigma;
}
