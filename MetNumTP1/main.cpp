/* 
 * File:   main.cpp
 * Author: nahdelgado
 *
 * Created on 31 de marzo de 2013, 22:19
 */

#include <cstdlib>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include "funciones.h"

using namespace std;

timeval start, end;
char num = '1';
char subnum = '1';
char letter = 'a';
char sep = ',';

void init_time()
{
    gettimeofday(&start, 0);
}

double get_time()
{
    gettimeofday(&end, 0);
    return (1000000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)) / 1000000.0;
}

int estimar(int argc, const char* argv[], bool csv) {    
    cout.precision(17);    
    
    if(!(argc == 7 || argc == 8))
    {
       printf("Los formatos posibles son:\n \'metnumtp1 FILENAME PRECISION {biseccion A B | newton ESTIMACION} MAXPASOS TOLERANCIA\'\n");       
       return 127;
    }
        
    int precision = atoi(argv[2]);
    
    if(precision > 52 || precision <= 0) {
        printf("La precisión debe ser un número entero entre 1 y 52.\n");
        return 2;
    }
    
    ifstream arch;
    arch.open(argv[1]);
    int n;
    arch >> n;
    
    TFloat muestra[n];
    double temp;
    int i = 0;
    while(i < n) {
        arch >> temp;
        muestra[i] = TFloat(temp, precision);
        i++;
    }
    
    arch.close();
    
    int m = 2;
    if(strcmp(argv[3], "biseccion") == 0)
    {
        m = 0;
    }
    
    if(strcmp(argv[3], "newton") == 0)
    {
        m = 1;
    }      
    
    TFloat beta = TFloat(0, precision);
    int pasos = 0;
    double tolerancia = 0.0;
        
    switch(m)
    {
        case 0:
        {
            if(argc != 8)
            {
                printf("El formato es:\n \'metnumtp1 FILENAME PRECISION biseccion A B MAXPASOS TOLERANCIA\'\n");
                return 124;
            }
            
            double dobA = atof(argv[4]);
            double dobB = atof(argv[5]);
            TFloat a = TFloat(dobA, precision);
            TFloat b = TFloat(dobB, precision);
            pasos = atoi(argv[6]);            
            
            tolerancia = atof(argv[7]);            
            
            beta = bisection(a, b, muestra, n, tolerancia, pasos);
            
            if(csv)
            {
                cout << argv[1] << sep << "biseccion" << sep << num << "." << subnum << "." << letter << sep << precision << " - " << tolerancia << sep << dobA << " - " << dobB << sep;
            }
            else
            {
                cout << "Archivo: " << argv[1] << " - Precision: " << precision << " - A: " << dobA << " - B: " << dobB << " - Iteraciones: " << pasos << " - Tolerancia: "<< tolerancia << endl;
            }
            
            break;        
        }        
        case 1:        
        {
            if(argc != 7)
            {                
                printf("El formato es:\n \'metnumtp1 FILENAME PRECISION newton ESTIMACION MAXPASOS TOLERANCIA\'\n");
                return 125;
            }
            
            double dobBeta = atof(argv[4]);
            beta = TFloat(dobBeta, precision);
            pasos = atoi(argv[5]);
            
            if(argc == 7)
            {
                tolerancia = atof(argv[6]);
            }            
                                  
            beta = newton(beta, muestra, n, tolerancia, pasos);
            
            if(csv)
            {
                cout << argv[1] << sep << "newton" << sep << num << "." << subnum << "." << letter << sep << precision << " - " << tolerancia << sep << dobBeta << sep;
            }
            else
            {
                cout << "Archivo: " << argv[1] << " - Precision: " << precision << " - Estimacion Inicial: " << dobBeta << " - Iteraciones: " << pasos << " - Tolerancia: "<< tolerancia << endl;
            }
            
            break;
        }
        default:
            printf("El metodo debe ser biseccion o newton, con sus correspondientes parametros\n");       
            return 126;
            break;
    }
    
    TFloat lambda = funcLambda(beta, muestra, n);
    TFloat sigma = funcSigma(beta, lambda, muestra, n);    
    
    if(csv)
    {
        cout << sigma.dbl() << sep << beta.dbl() << sep << lambda.dbl() << sep << pasos << sep;
    }
    else
    {
        cout << "sigma=" << fixed << sigma.dbl() << " / beta=" << fixed << beta.dbl() << " / lambda=" << fixed << lambda.dbl() << endl;
    }

    return 0;
}

int main (int argc, const char* argv[])
{
    if(argc != 2 || strcmp(argv[1], "experimento") != 0)
    {
        return estimar(argc, argv, false);
    }
    
    // El código que sigue es para automatizar nuestro experimento y generar
    // output en formato csv, que puede ser levantado en cualquier planilla
    // de calculo (Excel, LibreOffice)
    
    char const * files[] = { "x_15_9_3.txt", "x_2_62_35.txt", "X1.txt", "X2.txt", "X3.txt", "X4.txt", "X5.txt", "X6.txt", "X7.txt" };
    char const* precision[] = { "51" , "25", "9"  };
    char const* tolerancia[] = { "1e-14" , "1e-5", "1e-3" };
    char const* parametrosNewton[] = { "6", "8", "5", "5", "4", "5", "18", "5", "4" };
    char const* parametrosBisec1[] = { "2", "2", "1", "3", "0.5", "2", "1", "1", "1.5" };
    char const* parametrosBisec2[] = { "10", "10", "6", "8", "4", "6", "5", "10", "7" };
    char const* maxPasos = "5000";
    
    cout << "Archivo" << sep << "Método" << sep << "Indice" << sep << "Precision - Tolerancia" << sep << "Estimación inicial / a - b" << sep << "Sigma" << sep << "Beta" << sep << "Lambda"<< sep << "Iteraciones" << sep << "Tiempo de ejecucion" << endl;
    
    for(int f = 0; f < 9; f++)
    {
        for(int p= 0; p < 3; p++)
        {
            int tempArgc = 8;
            char const * tempArgv[8] = { argv[0], files[f], precision[p], "biseccion", parametrosBisec1[f], parametrosBisec2[f], maxPasos, tolerancia[p]  };
            init_time();
            estimar(tempArgc, tempArgv, true);
            double time = get_time();            
            cout << time << "s" << endl;
            letter += 1;
        }
        
        subnum += 1;
        letter = 'a';
        
        for(int p= 0; p < 3; p++)
        {
            int tempArgc = 7;
            char const * tempArgv[7] = { argv[0], files[f], precision[p], "newton", parametrosNewton[f], maxPasos, tolerancia[p]  };
            init_time();
            estimar(tempArgc, tempArgv, true);
            double time = get_time();            
            cout << time << "s" << endl;
            letter += 1;
        }
        
        letter = 'a';
        subnum = '1';
        num += 1;
    }
};


