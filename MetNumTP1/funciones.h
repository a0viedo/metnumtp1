/* 
 * File:   funciones.h
 * Author: nahdelgado
 *
 * Created on 31 de marzo de 2013, 22:21
 */

#ifndef FUNCIONES_H
#define	FUNCIONES_H

#include "TFloat.h"
#include <math.h>

TFloat funcLambda(TFloat beta, TFloat *muestra, int n);
TFloat funcSigma(TFloat beta, TFloat lambda, TFloat *muestra, int n);
TFloat newton(TFloat beta, TFloat *muestra, int n, double tolerancia, int &maxPasos);
TFloat bisection(TFloat a, TFloat b, TFloat *muestra, int n, double tolerancia, int& maxPasos);

#endif	/* FUNCIONES_H */
