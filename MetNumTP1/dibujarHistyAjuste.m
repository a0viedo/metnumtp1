function dibujarHistyAjuste(data, sigma1,beta1,lambda1, sigma2, beta2, lambda2, sigma3, beta3, lambda3)
%
% Dado un vector de datos data y los parametros sigma, beta y lambda de una
% distribucion Gamma Generalizada, se grafica el histograma de los datos y
% la funcion de densidad de la DGG.
%

x_bins = min(data) : 0.02 : max(data);

% cuento cant repeticiones en rangos delimitados por x_bins
count = histc(data,x_bins);

figure;''
bar(x_bins, count / sum(count),'facecolor','green');

hold on

if(not(or(or(or(or(or(or(or(or(sigma1 == 0, sigma1 == inf), isnan(sigma1)), beta1 == 0), beta1 == inf), isnan(beta1)), lambda1 == 0), lambda1 == inf),  isnan(lambda1))))
y = GGDpdf_c(x_bins,sigma1,beta1,lambda1);
plot(x_bins, y/sum(y),"r;Precision 51;","LineWidth",6)
endif

if (not(or(or(or(or(or(or(or(or(sigma2 == 0, sigma2 == inf), isnan(sigma2)), beta2 == 0), beta2 == inf), isnan(beta2)), lambda2 == 0), lambda2 == inf),  isnan(lambda2))))
	y = GGDpdf_c(x_bins,sigma2,beta2,lambda2);
	plot(x_bins, y/sum(y),"b;Precision 25;","LineWidth",3)
endif

if (not(or(or(or(or(or(or(or(or(sigma3 == 0, sigma3 == inf), isnan(sigma3)), beta3 == 0), beta3 == inf), isnan(beta3)), lambda3 == 0), lambda3 == inf),  isnan(lambda3))))
	y = GGDpdf_c(x_bins,sigma3,beta3,lambda3);
	plot(x_bins, y/sum(y),"k;Precision 9;","LineWidth",2)
endif

